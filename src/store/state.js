const state = {
    // portfolio:[
    //     { id:1,  quantity: 1},
    //     { id:2,  quantity: 2},
    //     { id:3,  quantity: 3},
    //     { id:4,  quantity: 4},
    // ],
    portfolio:[],    
    stocks:[
        { id: 1, name: 'BMW', price: 110 },
        { id: 2, name: 'Google', price: 200 },
        { id: 3, name: 'Apple', price: 250 },
        { id: 4, name: 'Twitter', price: 8 }
    ],    
    funds: 10000
}

export default state