const mutations = {
    BUY_STOCK(state, { id, price, quantity }){
        const record = state.portfolio.find(element => element.id == id);
        if (record) {
            record.quantity += quantity;
        } else {
            state.portfolio.push({ id, quantity,});
        }
        state.funds -= price * quantity;        
    },
    SELL_STOCK(state, { id, price, quantity}){
        const record = state.portfolio.find(item => {
            return item.id === id
        })
        if (record.quantity > quantity) {
            record.quantity -= quantity
        }else{
            state.portfolio.splice(state.portfolio.indexOf(record), 1)
        }
        state.funds += price * quantity
    },
    RND_STOCKS(state){
        state.stocks.forEach( item => {
            item.price = Math.round( item.price * ( Math.random() + 0.5 ) )
        })
    },
    LOAD_STOCK(state, { funds, portfolio, stocks }){
        state.funds = funds
        state.portfolio = portfolio
        state.stocks = stocks
    }
}

export default mutations