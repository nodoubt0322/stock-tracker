const getters = {
    stockPortfolio (state){
        return state.portfolio.map(item => {
            const record = state.stocks.find(stock=>{
                return stock.id === item.id
            })
            return {
                id: item.id,
                quantity: item.quantity,
                name: record.name,
                price: record.price
            }
        })
    }
}

export default getters